<?php

function node_subs_queue_add_to_process($node) {
  if (node_subs_check_node_type($node->type)) {
    $status = node_subs_check_node($node->nid);
    $message_options = array('!title' => $node->title);
    if ($status == 'ready' && $node->status) {
      node_subs_node_queue_add($node->nid);
      drupal_set_message(t('Node !title added form subscribe queue', $message_options));
    }
    elseif ($status == 'queue' && !$node->status) {
      node_subs_node_queue_delete($node->nid);
      drupal_set_message(t('Node !title removed form subscribe queue', $message_options));
    }
  }
}
