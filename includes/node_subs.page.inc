<?php

function node_subs_confirm_page() {
  $out = array();
  $out['text'] = array(
    '#prefix' => '<div id="node_subs-confirmation-text">',
    '#suffix' => '</div>',
    '#markup' => node_subs_get_text('confirmation')
  );
  return $out;
}

function node_subs_unsubscribe_page($user_token) {
  $email = isset($_GET['email']) ? $_GET['email'] : FALSE;
  $check = FALSE;
  if ($email) {
    if (!valid_email_address($email)) {
      watchdog('node_subs', 'Invalid email unsubscribe request. Email is !email, ip: !ip', array('!email' => $email, '!ip' => ip_address()), WATCHDOG_WARNING);
    }
    else {
      $account = node_subs_account_email_load($email);
      if (!$account) {
        watchdog('node_subs', 'Invalid account unsubscribe request. Email is !email, ip: !ip', array('!email' => $email, '!ip' => ip_address()), WATCHDOG_WARNING);
      }
      else {
        $check = node_subs_get_account_token($email) === $user_token;
        if (!$check) {
          $mail = $email ? $email : 'not defined';
          watchdog('node_subs', 'Invalid token unsubscribe request. Email is !email, ip: !ip', array('!email' => $mail, '!ip' => ip_address()), WATCHDOG_WARNING);
        }
      }
    }
  }
  if (!$check) {
    $out['error'] = array(
      '#markup' => t('We cant unsubscribe you.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>'
    );
    return $out;
  }
  if ($account->status) {
    $out['info'] = array(
      '#markup' => t('Email %email unsubscribed', array('%email' => $email)),
      '#prefix' => '<p>',
      '#suffix' => '</p>'
    );
    $account->status = 0;
    node_subs_account_save($account);
    watchdog('node_subs', 'User unsubscribed. Email is !email, ip: !ip', array('!email' => $email, '!ip' => ip_address()));
  }
  else {
    $out['info'] = array(
      '#markup' => t('Email %email alredy unsubscribed', array('%email' => $email)),
      '#prefix' => '<p>',
      '#suffix' => '</p>'
    );
  }
  return $out;
}
