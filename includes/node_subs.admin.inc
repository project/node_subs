<?php

function node_subs_settings_page_form($form, $form_state) {
  $form['node_subs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email settings'),
    '#collapsible' => FALSE
  );
  $form['node_subs']['node_subs_count_per_batch'] = array(
    '#type' => 'textfield',
    '#title' => t('Wich count emails send per 1 batch'),
    '#default_value' => variable_get('node_subs_count_per_batch', 100),
    '#required' => TRUE
  );
  $default_from = variable_get('site_mail', ini_get('sendmail_from'));

  $form['node_subs']['node_subs_from'] = array(
    '#type' => 'textfield',
    '#title' => t('From address'),
    '#default_value' => variable_get('node_subs_from', $default_from),
    '#required' => TRUE
  );
  $form['node_subs']['node_subs_is_crone'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send mails by cron'),
    '#default_value' => variable_get('node_subs_is_crone', 1),
    '#desciption' => t('Send mails by cron')
  );
  $form['node_subs']['node_subs_add_unsubscribe'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add unsubbcribe link to mail body'),
    '#default_value' => variable_get('node_subs_add_unsubscribe', 1),
    '#desciption' => t('Add unsubbcribe link to mail body')
  );
  $form['node_subs_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Message settings'),
    '#collapsible' => FALSE
  );
  $form['node_subs_message']['node_subs_allowed_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed tags for formating mail'),
    '#default_value' => variable_get('node_subs_allowed_tags', '<p><a><strong><br><i><em><b>')
  );

  $form['#validate'] = array('node_subs_settings_validation');
  return system_settings_form($form);
}

function node_subs_settings_validation(&$form, &$form_state) {
  $values = $form_state['values'];
  $per_batch = (int) $values['node_subs_count_per_batch'];
  if (((string) $per_batch !== $values['node_subs_count_per_batch']) || ($per_batch < 1)) {
    form_set_error('node_subs_count_per_batch', t('Only integer values available'));
  }
  if (!valid_email_address($values['node_subs_from'])) {
    form_set_error('node_subs_from', t('Type valid email address'));
  }
}

function node_subs_settings_text_form($form, $form_state) {
  $dir = drupal_get_path('module', 'node_subs') . '/misc/';
  $file = $dir . 'node_subs_text.ini';
  $default_texts = parse_ini_file($file);
  foreach ($default_texts as $key => $text) {
    $form_key = 'node_subs_text_' . $key;
    $default = variable_get($form_key, $text);
    $form[$form_key] = array(
      '#type' => 'textarea',
      '#title' => $key,
      '#default_value' => $default
    );
  }
  return system_settings_form($form);
}

function node_subs_settings_test_form($form, $form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'test',
    '#submit' => array('node_subs_queue_process')
  );
  return $form;
}

/**
 * Function for manage node_subs subscribers list.
 */
function node_subs_users_list_page() {
  $options = array();
// Get subscribers from db
  $conditions = array();
  $conditions['deleted'] = 0;
  $subscribers = node_subs_account_load_multiple(array(), $conditions, 10);

// Creating table header
  $header = array('name' => t('Name'), 'email' => t('Email'), 'status' => t('Status'), 'action' => t('Actions'));

// Creating rows of subscribers
  foreach ($subscribers as $subscrs_ind => $subscr) {
    $options[$subscrs_ind]['name'] = $subscr->name;
    $options[$subscrs_ind]['email'] = $subscr->email;
    $options[$subscrs_ind]['status'] = $subscr->status ? t('Subscribed') : t('Unsubscribed');

    $options[$subscrs_ind]['action']['data'] = array(
      '#theme' => 'links',
      '#links' => array(
        'edit' => array(
          'title' => t('Edit'),
          'href' => NODE_SUBS_ADMIN_PATH . '/' . $subscr->id . '/edit'
        ),
        'delete' => array(
          'title' => t('Delete'),
          'href' => NODE_SUBS_ADMIN_PATH . '/' . $subscr->id . '/delete'
        ),
      ),
    );
  }

  $form = array();

// Table for output
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('Subscribers not found.'),
    '#attributes' => array('class' => array('node-subs-subscribers'))
  );

  $form['select'] = array(
    '#type' => 'select',
    '#title' => t('Select the action for the selected items'),
    '#options' => array(
      'unsubscribe' => t('Unsubscribe'),
      'subscribe' => t('Subscribe'),
      'delete' => t('Delete')
    ),
    '#empty_option' => t('- Select -'),
    '#required' => TRUE,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );
  $form['pager'] = array('#theme' => 'pager');
  return $form;
}

function node_subs_users_list_page_validate(&$form, &$form_state) {
  $selected_items = array_filter($form_state['values']['table']);
  if (empty($selected_items)) {
    form_set_error('table', t('No items selected.'));
  }
}

function node_subs_users_list_page_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $selected_ids = array_filter($form_state['values']['table']);
  $action = $form_state['values']['select'];
  $subscribers = node_subs_account_load_multiple($selected_ids);

  switch ($action) {
    case 'unsubscribe':
      foreach ($subscribers as $subscriber) {
        $subscriber->status = 0;
        node_subs_account_save($subscriber);
      }
      $message = format_plural(count($subscribers), 'Unsubscribed 1 user', 'Unsubscribed @count users');
      drupal_set_message($message);
      break;
    case 'subscribe':
      foreach ($subscribers as $subscriber) {
        $subscriber->status = 1;
        node_subs_account_save($subscriber);
      }
      $message = format_plural(count($subscribers), 'Subscribed 1 user', 'Subscribed @count users');
      drupal_set_message($message);
      break;
    case 'delete':
      foreach ($subscribers as $subscriber) {
        node_subs_account_delete($subscriber);
      }
      $message = format_plural(count($subscribers), 'Deleted 1 user', 'Deleted @count users');
      drupal_set_message($message);
      break;
  }
}

function node_subs_subscriber_form($form, $form_state, $subscriber = NULL) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $subscriber ? $subscriber->name : '',
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => $subscriber ? $subscriber->email : '',
    '#required' => true,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => $subscriber ? $subscriber->status : 1,
  );

  if ($subscriber) {
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $subscriber->id,
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $subscriber ? t('Update') : t('Save'),
  );

  return $form;
}

function node_subs_subscriber_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['email'])) {
    $email = $form_state['values']['email'];
    if (!valid_email_address($email)) {
      form_set_error('email', t('You must enter a valid e-mail address.'));
    }
    else {
      $exists_account = node_subs_account_email_load($email, array('deleted' => 0));
      if ($exists_account) {
        form_set_error('email', t('The e-mail address %email is already taken.', array('%email' => $email)));
      }
    }
  }
}

function node_subs_subscriber_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $subscriber = (object) $form_state['values'];

  if (isset($subscriber->id)) {
    $message = t('Subscriber @name (@email) updated', array(
      '@name' => $subscriber->name,
      '@email' => $subscriber->email
    ));
  }
  else {
    $message = t('Subscriber @name (@email) created', array(
      '@name' => $subscriber->name,
      '@email' => $subscriber->email
    ));
  }
  node_subs_account_save($subscriber, FALSE);
  drupal_set_message($message);
  $form_state['redirect'] = NODE_SUBS_ADMIN_PATH;
}

function node_subs_subscriber_delete_form($form, $form_state, $subscriber) {
  $form['subscriber'] = array(
    '#type' => 'value',
    '#value' => $subscriber,
  );

  return confirm_form($form, t('Are you sure you want to delete the %name (%email)?', array('%name' => $subscriber->name, '%email' => $subscriber->email)), NODE_SUBS_ADMIN_PATH, NULL, t('Delete'));
}

function node_subs_subscriber_delete_form_submit($form, &$form_state) {
  $subscriber = $form_state['values']['subscriber'];
  drupal_set_message(t('Account %name (%email) deleted.', array('%name' => $subscriber->name, '%email' => $subscriber->email)));
  $form_state['redirect'] = NODE_SUBS_ADMIN_PATH;
  node_subs_account_delete($subscriber);
}

function node_subs_queue_form() {
  $node_subs_queue_nodes = node_subs_get_gueue_nodes(NODE_SUBS_QUEUE_TABLE);
// Creating table header
  $header = array(
    'title' => t('Title'),
    'type' => t('Node type'),
    'created' => t('Created'),
    'process' => t('Process'),
  );

  $options = array();

  if (!empty($node_subs_queue_nodes)) {
    foreach ($node_subs_queue_nodes as $node) {
      $node_obj = node_load($node->nid);
      if (!$node_obj) {
        continue;
      }
      $options[$node->nid] = array(
        'title' => $node_obj->title,
        'type' => $node_obj->type,
        'created' => format_date($node_obj->created, 'custom', 'd.m.Y - H:i'),
        'process' => node_subs_count_accounts(TRUE, $node->process) . ' / ' . node_subs_count_accounts(),
      );
    }
  }

// Table for output
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('Queue is empty.'),
    '#attributes' => array('class' => array('node-subs-queue'))
  );

  $form['select'] = array(
    '#type' => 'select',
    '#title' => t('Select the action for the selected items'),
    '#options' => array(
      'remove_queue' => t('Remove form queue'),
    ),
    '#empty_option' => t('- Select -'),
    '#required' => TRUE,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  $form['pager'] = array('#theme' => 'pager');

  return $form;
}

function node_subs_queue_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $node_ids = array_filter($form_state['values']['table']);
  $action = $form_state['values']['select'];

  switch ($action) {
    case 'remove_queue':
      foreach ($node_ids as $nid) {
        node_subs_node_move_to_history($nid);
      }
      $message = format_plural(count($node_ids), 'Archived 1 node', 'Archived @count nodes');
      drupal_set_message($message);
      break;
  }
}

function node_subs_archive() {
  $header = array('nid' => 'Nid', 'title' => t('Title'), 'type' => t('Node type'), 'created' => t('Created'), 'sended' => t('Sended'));
  $rows = array();

  $history_nodes = node_subs_get_history_nodes(NODE_SUBS_HISTORY_TABLE);
  if (!empty($history_nodes)) {
    foreach ($history_nodes as $node) {
      $node_obj = node_load($node->nid);
      if (!$node_obj) {
        continue;
      }
      $rows[] = array(
        'nid' => $node_obj->nid,
        'title' => $node_obj->title,
        'type' => $node_obj->type,
        'created' => format_date($node->created, 'custom', 'd.m.Y - H:i'),
        'sended' => format_date($node->send, 'custom', 'd.m.Y - H:i'),
      );
    }
  }

// Table for output
  $output['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Arhive is empty.'),
    '#attributes' => array('class' => array('node-subs-archive'))
  );

  $output['pager'] = array('#theme' => 'pager');

  return $output;
}
